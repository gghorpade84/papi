package com.Papi.api;

import com.qaprosoft.carina.core.foundation.api.AbstractApiMethodV2;
import com.qaprosoft.carina.core.foundation.utils.Configuration;

public class Lowbalance extends AbstractApiMethodV2 {

	public Lowbalance() {

		super(null, null);
		replaceUrlPlaceholder("base_url", Configuration.getEnvArg("api_url"));

	}

}
