package com.Papi.api;

import com.qaprosoft.carina.core.foundation.api.AbstractApiMethodV2;
import com.qaprosoft.carina.core.foundation.utils.Configuration;

public class WhatsNewDataMethod extends AbstractApiMethodV2 {

	public WhatsNewDataMethod(String userType) {

		super(null, null);
		replaceUrlPlaceholder("base_url", Configuration.getEnvArg("api_url"));
		replaceUrlPlaceholder("userType", userType);

	}

}
