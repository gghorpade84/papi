package com.Papi.api;

import java.util.Properties;

import com.qaprosoft.carina.core.foundation.api.AbstractApiMethodV2;
import com.qaprosoft.carina.core.foundation.utils.Configuration;

public class AirtimeNotification extends AbstractApiMethodV2 {

	public AirtimeNotification() {

		super(null, null,new Properties());
		
		replaceUrlPlaceholder("base_url", Configuration.getEnvArg("api_url"));

	}

}
