package com.Papi.api;

import com.qaprosoft.carina.core.foundation.api.AbstractApiMethodV2;
import com.qaprosoft.carina.core.foundation.utils.Configuration;

public class ToggleAllNotifications extends AbstractApiMethodV2 {

	   public ToggleAllNotifications() 
	   {
	        super("api/users/_post/toggleNoti_rq.json",null, "api/users/toggleNoti.properties");          
	        replaceUrlPlaceholder("base_url", Configuration.getEnvArg("api_url"));
	    }
	
	

}
