package com.Papi.api;

import com.qaprosoft.carina.core.foundation.api.AbstractApiMethodV2;
import com.qaprosoft.carina.core.foundation.utils.Configuration;

public class NotificationState  extends AbstractApiMethodV2 {

	   public NotificationState() 
	   {
	        super("api/users/_post/notification_rq.json",null, "api/users/notification.properties");          
	        replaceUrlPlaceholder("base_url", Configuration.getEnvArg("api_url"));
	    }
	
	

}
