package com.Papi.api;

import com.qaprosoft.carina.core.foundation.api.AbstractApiMethodV2;
import com.qaprosoft.carina.core.foundation.utils.Configuration;

public class GetNotificationData extends AbstractApiMethodV2 {

	   public GetNotificationData() 
	   {
	        super("api/users/_post/getNoti_rq.json",null, "api/users/getNoti.properties");          
	        replaceUrlPlaceholder("base_url", Configuration.getEnvArg("api_url"));
	    }
	

}
