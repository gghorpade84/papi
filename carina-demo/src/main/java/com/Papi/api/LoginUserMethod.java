package com.Papi.api;

import com.qaprosoft.carina.core.foundation.api.AbstractApiMethodV2;
import com.qaprosoft.carina.core.foundation.utils.Configuration;

public class LoginUserMethod extends AbstractApiMethodV2 {

   public LoginUserMethod() 
   {
        super("api/users/_post/login_rq.json",null, "api/users/login.properties");          
        replaceUrlPlaceholder("base_url", Configuration.getEnvArg("api_url"));
    }
}