package com.Papi.api.test;

import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.Papi.api.AirtimeNotification;
import com.Papi.api.Balance;
import com.Papi.api.GetNotificationData;
import com.Papi.api.Journal;
import com.Papi.api.LoginUserMethod;
import com.Papi.api.Lowbalance;
import com.Papi.api.NotificationState;
import com.Papi.api.WhatsNewDataMethod;
import com.Papi.api.ToggleAllNotifications;
import com.Papi.api.UpdateNotification;
import com.amazonaws.Response;
import com.qaprosoft.carina.core.foundation.IAbstractTest;
import com.qaprosoft.carina.core.foundation.api.AbstractApiMethod;
import com.qaprosoft.carina.core.foundation.api.http.HttpResponseStatusType;
import com.qaprosoft.carina.core.foundation.dataprovider.annotations.XlsDataSourceParameters;
import com.qaprosoft.carina.core.foundation.utils.ownership.MethodOwner;

import io.restassured.path.json.JsonPath;

public class LoginUserTest implements IAbstractTest {

	String B_token;

	@Test(dataProvider = "SingleDataProvider", description = "Testing Login APi")
	@MethodOwner(owner = "Beauto")
	@XlsDataSourceParameters(path = "xls/TDD.xlsx", sheet = "Login", dsUid = "TC", dsArgs = "email,password")
	public void Login_user(String email, String password) {
		LoginUserMethod login = new LoginUserMethod();

		// calling Api
		login.getProperties().replace("email", email);
		login.getProperties().replace("password", password);
		String tokenResp = login.callAPI().asString();

		// expect status
		login.expectResponseStatus(HttpResponseStatusType.OK_200);

		JsonPath jpath = new JsonPath(tokenResp);
		tokenResp = jpath.get("token");

		B_token = tokenResp;
		System.out.println("Token=" + B_token);
	}

	@Test(dataProvider = "SingleDataProvider", description = "Testing WhatsNew APi")
	@MethodOwner(owner = "Beauto")
	@XlsDataSourceParameters(path = "xls/TDD.xlsx", sheet = "WhatsNew", dsUid = "TC", dsArgs = "userType")
	public void whatsNew(String userType) {

		WhatsNewDataMethod whatsNew = new WhatsNewDataMethod(userType);
		whatsNew.setHeaders("Authorization=Bearer " + B_token);
		whatsNew.callAPI();

	}

	@Test(dataProvider = "SingleDataProvider", description = "Testing Notification state APi")
	@MethodOwner(owner = "Beauto")
	@XlsDataSourceParameters(path = "xls/TDD.xlsx", sheet = "NotiState", dsUid = "TC", dsArgs = "emailAddress,flag,msisdn,partnerCode,userId")
	public void notification_State(String emailAddress, String flag, String msisdn, String partnerCode, String userId) {

		NotificationState state = new NotificationState();
		state.getProperties().replace("emailAddress", emailAddress);
		state.getProperties().replace("flag", flag);
		state.getProperties().replace("msisdn", msisdn);
		state.getProperties().replace("partnerCode", partnerCode);
		state.getProperties().replace("userId", userId);

		state.setHeaders("Authorization=Bearer " + B_token);
		state.callAPI();

	}

	@Test(dataProvider = "SingleDataProvider", description = "Testing Toggle notification APi")
	@MethodOwner(owner = "Beauto")
	@XlsDataSourceParameters(path = "xls/TDD.xlsx", sheet = "toggleNoti", dsUid = "TC", dsArgs = "partnerCode,userId,toggleStatus")
	public void toggle_AllNotification(String partnerCode, String userId, String toggleStatus) {

		ToggleAllNotifications toggle = new ToggleAllNotifications();

		toggle.getProperties().replace("partnerCode", partnerCode);
		toggle.getProperties().replace("userId", userId);
		toggle.getProperties().replace("toggleStatus", toggleStatus);

		toggle.setHeaders("Authorization=Bearer " + B_token);
		toggle.callAPI();

	}

	@Test(dataProvider = "SingleDataProvider", description = "Testing Get notification APi")
	@MethodOwner(owner = "Beauto")
	@XlsDataSourceParameters(path = "xls/TDD.xlsx", sheet = "getNoti", dsUid = "TC", dsArgs = "partnerCode,userId,notificationSetupId")
	public void get_Notification(String partnerCode, String userId, String notificationSetupId) {

		GetNotificationData get = new GetNotificationData();

		get.getProperties().replace("partnerCode", partnerCode);
		get.getProperties().replace("userId", userId);
		get.getProperties().replace("notificationSetupId", notificationSetupId);

		get.setHeaders("Authorization=Bearer " + B_token);
		get.callAPI();

	}

	@Test(dataProvider = "SingleDataProvider", description = "Testing Update notification APi")
	@MethodOwner(owner = "Beauto")
	@XlsDataSourceParameters(path = "xls/TDD.xlsx", sheet = "UpdateNoti", dsUid = "TC", dsArgs = "notificationSetupId,partnerCode,status,notificationScheduleFrequency,deliveryMethod,notificationStartDate,notificationEndDate")
	public void update_Notification(String notificationSetupId, String partnerCode, String status,
			String notificationScheduleFrequency, String deliveryMethod, String notificationStartDate,
			String notificationEndDate) {

		UpdateNotification update = new UpdateNotification();

		update.getProperties().replace("notificationSetupId", notificationSetupId);
		update.getProperties().replace("partnerCode", partnerCode);
		update.getProperties().replace("status", status);
		update.getProperties().replace("notificationScheduleFrequency", notificationScheduleFrequency);
		update.getProperties().replace("deliveryMethod", deliveryMethod);
		update.getProperties().replace("notificationStartDate", notificationStartDate);
		update.getProperties().replace("notificationEndDate", notificationEndDate);

		update.setHeaders("Authorization=Bearer " + B_token);
		update.callAPI();

	}

	@Test(dataProvider = "SingleDataProvider", description = "Testing Low Balance APi")
	@MethodOwner(owner = "Beauto")
	//@XlsDataSourceParameters(path = "xls/TDD.xlsx", sheet = "WhatsNew", dsUid = "TC", dsArgs = "userType")
	public void low_balance() {

		Lowbalance lowBalance = new Lowbalance();
		lowBalance.setHeaders("Authorization=Bearer " + B_token);
		lowBalance.callAPI();

	}

	@Test(dataProvider = "SingleDataProvider", description = "Testing Balnace APi")
	@MethodOwner(owner = "Beauto")
//	@XlsDataSourceParameters(path = "xls/TDD.xlsx", sheet = "WhatsNew", dsUid = "TC", dsArgs = "userType")
	public void balance() {

		Balance bal = new Balance();
		bal.setHeaders("Authorization=Bearer " + B_token);
		bal.callAPI();

	}

	@Test(dataProvider = "SingleDataProvider", description = "Testing Journal APi")
	@MethodOwner(owner = "Beauto")
	//@XlsDataSourceParameters(path = "xls/TDD.xlsx", sheet = "WhatsNew", dsUid = "TC", dsArgs = "userType")
	public void journal() {

		Journal journal = new Journal();
		journal.setHeaders("Authorization=Bearer " + B_token);
		journal.callAPI();

	}

	@Test(dataProvider = "SingleDataProvider", description = "Testing Airtime Notification APi")
	@MethodOwner(owner = "Beauto")
	//@XlsDataSourceParameters(path = "xls/TDD.xlsx", sheet = "WhatsNew", dsUid = "TC", dsArgs = "userType")
	public void airtime_notification() {

		AirtimeNotification airtime = new AirtimeNotification();
		airtime.setHeaders("Authorization=Bearer " + B_token);
		airtime.callAPI();

	}
}
